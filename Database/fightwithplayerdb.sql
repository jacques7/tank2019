-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.8-MariaDB
-- PHP のバージョン: 7.2.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `tank`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `fightwithplayerdb`
--

CREATE TABLE `fightwithplayerdb` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `record` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `fightwithplayerdb`
--

INSERT INTO `fightwithplayerdb` (`id`, `name`, `record`, `date`) VALUES
(1, '一番目', '5:0', '2019-12-03 16:36:03'),
(2, '二番目', '4:1', '2019-12-03 16:36:03'),
(3, '二番目の名前', '3:2', '2019-12-05 05:01:12'),
(4, '新し名前', '0:5', '2019-12-05 05:45:31'),
(6, 'また', '0:5', '2019-12-05 05:48:35'),
(7, 'taruma', '0:5', '2019-12-05 05:50:35'),
(9, 'やった', '0:5', '2019-12-05 06:54:32'),
(10, 'もう一度', '0:5', '2019-12-05 06:55:08'),
(11, '再来一个', '0:5', '2019-12-05 07:01:02'),
(15, '噢了', '1:5', '2019-12-12 08:29:33'),
(16, 'matamata', '0:0', '2019-12-12 08:32:36'),
(17, 'hai', '0:5', '2019-12-12 08:34:56'),
(18, '木木木ssss', '0:5', '2019-12-13 01:36:23'),
(21, 'YOUYOUO', '3:5', '2019-12-14 22:26:56');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `fightwithplayerdb`
--
ALTER TABLE `fightwithplayerdb`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `fightwithplayerdb`
--
ALTER TABLE `fightwithplayerdb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
