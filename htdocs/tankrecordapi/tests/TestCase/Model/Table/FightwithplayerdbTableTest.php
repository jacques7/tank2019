<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FightwithplayerdbTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FightwithplayerdbTable Test Case
 */
class FightwithplayerdbTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FightwithplayerdbTable
     */
    public $Fightwithplayerdb;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Fightwithplayerdb'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Fightwithplayerdb') ? [] : ['className' => FightwithplayerdbTable::class];
        $this->Fightwithplayerdb = TableRegistry::getTableLocator()->get('Fightwithplayerdb', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fightwithplayerdb);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
