<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fightwithplayerdb Model
 *
 * @method \App\Model\Entity\Fightwithplayerdb get($primaryKey, $options = [])
 * @method \App\Model\Entity\Fightwithplayerdb newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Fightwithplayerdb[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Fightwithplayerdb|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fightwithplayerdb saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fightwithplayerdb patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Fightwithplayerdb[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Fightwithplayerdb findOrCreate($search, callable $callback = null, $options = [])
 */
class FightwithplayerdbTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fightwithplayerdb');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 30)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('record')
            ->maxLength('record', 30)
            ->requirePresence('record', 'create')
            ->notEmptyString('record');

        $validator
            ->dateTime('date')
            ->notEmptyDateTime('date');

        return $validator;
    }
}
