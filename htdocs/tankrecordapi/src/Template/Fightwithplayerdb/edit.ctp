<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fightwithplayerdb $fightwithplayerdb
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $fightwithplayerdb->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $fightwithplayerdb->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Fightwithplayerdb'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="fightwithplayerdb form large-9 medium-8 columns content">
    <?= $this->Form->create($fightwithplayerdb) ?>
    <fieldset>
        <legend><?= __('Edit Fightwithplayerdb') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('record');
            echo $this->Form->control('date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
