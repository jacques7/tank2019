<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fightwithplayerdb $fightwithplayerdb
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Fightwithplayerdb'), ['action' => 'edit', $fightwithplayerdb->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Fightwithplayerdb'), ['action' => 'delete', $fightwithplayerdb->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fightwithplayerdb->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fightwithplayerdb'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fightwithplayerdb'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="fightwithplayerdb view large-9 medium-8 columns content">
    <h3><?= h($fightwithplayerdb->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($fightwithplayerdb->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Record') ?></th>
            <td><?= h($fightwithplayerdb->record) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($fightwithplayerdb->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($fightwithplayerdb->date) ?></td>
        </tr>
    </table>
</div>
