<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fightwithplayerdb Controller
 *
 * @property \App\Model\Table\FightwithplayerdbTable $Fightwithplayerdb
 *
 * @method \App\Model\Entity\Fightwithplayerdb[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightwithplayerdbController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $fightwithplayerdb = $this->paginate($this->Fightwithplayerdb);

        $this->set(compact('fightwithplayerdb'));
    }

    /**
     * View method
     *
     * @param string|null $id Fightwithplayerdb id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fightwithplayerdb = $this->Fightwithplayerdb->get($id, [
            'contain' => []
        ]);

        $this->set('fightwithplayerdb', $fightwithplayerdb);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fightwithplayerdb = $this->Fightwithplayerdb->newEntity();
        if ($this->request->is('post')) {
            $fightwithplayerdb = $this->Fightwithplayerdb->patchEntity($fightwithplayerdb, $this->request->getData());
            if ($this->Fightwithplayerdb->save($fightwithplayerdb)) {
                $this->Flash->success(__('The fightwithplayerdb has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fightwithplayerdb could not be saved. Please, try again.'));
        }
        $this->set(compact('fightwithplayerdb'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Fightwithplayerdb id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fightwithplayerdb = $this->Fightwithplayerdb->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fightwithplayerdb = $this->Fightwithplayerdb->patchEntity($fightwithplayerdb, $this->request->getData());
            if ($this->Fightwithplayerdb->save($fightwithplayerdb)) {
                $this->Flash->success(__('The fightwithplayerdb has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fightwithplayerdb could not be saved. Please, try again.'));
        }
        $this->set(compact('fightwithplayerdb'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Fightwithplayerdb id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fightwithplayerdb = $this->Fightwithplayerdb->get($id);
        if ($this->Fightwithplayerdb->delete($fightwithplayerdb)) {
            $this->Flash->success(__('The fightwithplayerdb has been deleted.'));
        } else {
            $this->Flash->error(__('The fightwithplayerdb could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getMessages()
    {
        error_log("getMessages()");

        $this->autoRender =false;

        $id = $this->request->getData("id");

        $query = $this->Fightwithplayerdb->find("all");

        $json_array = json_encode($query);

        echo $json_array;
    }

    public function setMessage()
    {
        error_log("setMessage()");
        date_default_timezone_set('Asia/Tokyo');

        $this->autoRender = false;

        $name = $this->request->getData("name");
        $record = $this->request->getData("record");

        // $name = "";
        // if(isset($this->request->data['Name']))
        // {
        //     $name = $this->request->data['Name'];
        //     error_log($name);
        // }

        // $message = "";
        // if(isset($this->request->data["Message"]))
        // {
        //     $message = $this->request->data["Message"];
        //     error_log($message);
        // }

        $data = array('name' => $name, 'record' => $record, 'date' => date('Y/m/d H:i:s'));

        $fightwithplayerdb = $this->Fightwithplayerdb->newEntity();
        $fightwithplayerdb = $this->Fightwithplayerdb->patchEntity($fightwithplayerdb, $data);

        if($this->Fightwithplayerdb->save($fightwithplayerdb))
        {
            echo "1";
        }
        else {
            echo "0";
        }
    }
}
