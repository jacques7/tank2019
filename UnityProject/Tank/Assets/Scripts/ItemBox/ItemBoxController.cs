﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBoxController : MonoBehaviour
{
    public LayerMask m_TankMask;
    
    private string itemType = default;              // アイテム種別
    private MeshRenderer[] renderers = default;     // アイテム色をコントロール
    private float m_Radius = 4.0f;                  // 感知範囲
    private int m_ColorIndex = 0;                   // 色別

    // Start is called before the first frame update
    void Start()
    {
        // 三種類色のアイテムをランダムに生成する
        m_ColorIndex = Random.Range(0, 3);
        renderers = gameObject.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < renderers.Length; i++)
        {
            if(m_ColorIndex == 0)
            {
                renderers[i].material.color = Color.green;
            }
            else if(m_ColorIndex == 1)
            {
                renderers[i].material.color = Color.blue;
            }
            else if(m_ColorIndex == 2)
            {
                renderers[i].material.color = Color.red;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //　アイテムボックスを回転する
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        // find which tank takes the itembox
        if(other.tag == "Player")
        {
            //　シールドがない場合
            Rigidbody targetRigidbody = other.GetComponent<Rigidbody>();

            TankItemManager targetItem = targetRigidbody.GetComponent<TankItemManager>();

            //　アイテム種類を判別
            this.itemType = GetItemBoxType();
            //　アイテムを拾った処理
            targetItem.HandleItem(this.itemType);

            //　アイテムを削除
            Destroy(gameObject);
        }
        else if(other.tag == "Shield")
        {
            //　シールドがある場合
            Collider[] colliders = Physics.OverlapSphere(transform.position, m_Radius, m_TankMask);

            for (int i = 0; i < colliders.Length; i++)
            {
                if(colliders[i].tag == "Player")
                {
                    Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

                    TankItemManager targetItem = targetRigidbody.GetComponent<TankItemManager>();

                    this.itemType = GetItemBoxType();
                    targetItem.HandleItem(this.itemType);

                    Destroy(gameObject);
                }
            }
        }
    }

    private string GetItemBoxType()
    {
        //　アイテム種類を判別、色分けて区別
        if (renderers[0].material.color == Color.green)
        {
            return "health";
        }
        else if (renderers[0].material.color == Color.blue)
        {
            return "shield";
        }
        else if (renderers[0].material.color == Color.red)
        {
            return "weapon";
        }

        return string.Empty;
    }
}
