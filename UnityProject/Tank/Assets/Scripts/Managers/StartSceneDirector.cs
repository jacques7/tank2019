﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneDirector : MonoBehaviour
{
    public void OnClickPlayerBtn()
    {
        SceneManager.LoadScene("Main");
    }

    public void OnClickComputerBtn()
    {
        SceneManager.LoadScene("Main 1");
    }
}
