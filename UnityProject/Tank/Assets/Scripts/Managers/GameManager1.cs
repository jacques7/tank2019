﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System.Collections.Generic;
using System.Linq;

using ArtificialTankDriver_by_QI;
using ALGORITHM.AI;

using MiniJSON;
using System.IO;

public class GameManager1 : MonoBehaviour
{
    [SerializeField] private int m_GeneticNum = 0;
    public int m_NumRoundsToWin = 5;        
    public float m_StartDelay = 3f;         
    public float m_EndDelay = 3f;           
    public CameraControl m_CameraControl;   
    public Text m_MessageText;              
    public GameObject m_TankPrefab;         
    public TankManager[] m_Tanks;           


    private int m_RoundNumber;              
    private WaitForSeconds m_StartWait;     
    private WaitForSeconds m_EndWait;       
    private string m_RoundWinner = "null";
    private string m_GameWinner = "null";

    [SerializeField] private LayerMask m_ObjectMask;
    private int m_AITankCount = 1;

    public GameObject m_AITankPrefab;
    public Transform m_SpawnPoint;

    private readonly List<Transform> m_initPosition = new List<Transform>();
    private readonly List<TankDriver> m_drivers = new List<TankDriver>();

    [HideInInspector] public GeneticOptimisation m_evolver;

    private float m_DetectRdius = 5.0f;

    private bool isGamePlaying = false;

    private void Start()
    {
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);

        SpawnAllTanks();
        GenerateInitial();
        SetCameraTargets();

        StartCoroutine(GameLoop());
    }

    private void Update()
    {
        if (isGamePlaying == true)
        {
            AIUpdate();
        }
    }

    private void AIUpdate()
    {
        foreach (var tankDriver in m_drivers)
        {
            tankDriver.DoSomethingUseful();
        }
    }


    private void SpawnAllTanks()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].m_Instance =
                Instantiate(m_TankPrefab, m_Tanks[i].m_SpawnPoint.position, m_Tanks[i].m_SpawnPoint.rotation) as GameObject;
            m_Tanks[i].m_PlayerNumber = i + 1;
            m_Tanks[i].Setup();
        }
    }

    public void GenerateInitial()
    {
        m_evolver = new GeneticOptimisation(m_AITankCount, 0.15, SelectionMethod.Natural);

        for (var i = 0; i < m_AITankCount; i++)
        {
            m_initPosition.Add(m_SpawnPoint);

            m_drivers.Add(Instantiate(m_AITankPrefab, m_SpawnPoint.position, m_SpawnPoint.rotation).GetComponent<TankDriver>());
            m_drivers[i].GetComponent<AITankManager>().Setup(i);            
            m_evolver.population.Add(m_drivers[i].network);
        }

        GetPopulationGenetic();
    }

    private void GetPopulationGenetic()
    {
        // 訓練結果を利用
        //GetGeneticFromFile();
        // 記録した遺伝子を利用
        GetGeneticFromRecord();
    }

    private void GetGeneticFromRecord()
    {
        var jsonInside = new GeneticRecord();
        var parsedValueInside = (IList)Json.Deserialize(jsonInside.Genetics);

        foreach (IDictionary resultOne in parsedValueInside)
        {
            var temp = m_evolver.population[0].OptimizeableValues;
            for (var i = 0; i < resultOne.Count - 1; i++)
            {
                temp[i] = (double)resultOne[$"OptimizeableValues[{i}]"];
            }
            m_evolver.population[0].OptimizeableValues = temp;
        }
    }

    private void GetGeneticFromFile()
    {
        // ローカルストレージパスを取得
        // windows環境だと [C: \Users\(ユーザー名)\AppData\LocalLow\DefaultCompany\(プロジェクト名) が指定される。
        string storagePath = Application.persistentDataPath;

        // jsonファイル名を定義
        const string nameJson = "localsave_geneticData01d.txt";

        // アクセスパスを作成
        string accessPath = $"{storagePath}/{nameJson}";

        // パスが存在するか確認
        if (!File.Exists(accessPath))
        {
            //return $"Path:\n{accessPath}\nNotFound!";
        }

        // jsonファイルを読み込む
        string json = File.ReadAllText(accessPath, System.Text.Encoding.UTF8);

        // デシリアライズを行い、Textエリア表示用に整形する。
        Deserialize(json);
    }

    private void Deserialize(string sStrJson)
    {
        // JSONデータは最初は配列から始まるので、Deserialize（デコード）した直後にリストへキャスト      
        var parsedValue = (IList)Json.Deserialize(sStrJson);
        string jsonInside = Json.Serialize(parsedValue[m_GeneticNum]);
        jsonInside = "[" + jsonInside + "]";
        var parsedValueInside = (IList)Json.Deserialize(jsonInside);

        foreach (IDictionary resultOne in parsedValueInside)
        {
            var temp = m_evolver.population[0].OptimizeableValues;
            for (var i = 0; i < resultOne.Count - 1; i++)
            {
                temp[i] = (double)resultOne[$"OptimizeableValues[{i}]"];
            }
            m_evolver.population[0].OptimizeableValues = temp;
        }
    }

    private void SetCameraTargets()
    {
        Transform[] targets = new Transform[m_Tanks.Length + m_drivers.Count];

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            targets[i] = m_Tanks[i].m_Instance.transform;
        }
        for (int i = m_Tanks.Length; i < m_Tanks.Length + m_drivers.Count; i++)
        {
            targets[i] = m_drivers[i- m_Tanks.Length].GetComponent<Transform>(); 
        }

        m_CameraControl.m_Targets = targets;
    }


    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundEnding());

        if (m_GameWinner != "null")
        {
            SaveScore();
            SceneManager.LoadScene("Record");
        }
        else
        {
            StartCoroutine(GameLoop());
        }
    }

    private void SaveScore()
    {
        GlobalController.Instance.m_PlayerScore = m_Tanks[0].m_Wins;
        GlobalController.Instance.m_AIScore = m_drivers[0].m_Wins;
    }


    private IEnumerator RoundStarting()
    {
        ResetAllTanks();
        RestoreInitial();
        DisableTankControl();

        isGamePlaying = false;

        m_CameraControl.SetStartPositionAndSize();

        m_RoundNumber++;
        m_MessageText.text = "ROUND" + m_RoundNumber;

        yield return m_StartWait;
    }


    private IEnumerator RoundPlaying()
    {
        EnableTankControl();

        isGamePlaying = true;

        m_MessageText.text = string.Empty;

        while (!OneTankLeft())
        {
            yield return null;
        }
    }


    private IEnumerator RoundEnding()
    {
        DisableTankControl();

        isGamePlaying = false;

        m_RoundWinner = null;

        m_RoundWinner = GetRoundWinner();

        if (m_RoundWinner == "Player")
        {
            m_Tanks[0].m_Wins++;
        }
        else if(m_RoundWinner == "AI")
        {
            m_drivers[0].m_Wins++;
        }

        m_GameWinner = GetGameWinner();

        string message = EndMessage();
        m_MessageText.text = message;

        yield return m_EndWait;
    }


    private bool OneTankLeft()
    {
        int numTanksLeft = 0;

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Instance.activeSelf)
                numTanksLeft++;
        }
        for (int i = 0; i < m_drivers.Count; i++)
        {
            if (m_drivers[i].gameObject.activeSelf)
                numTanksLeft++;
        }

        return numTanksLeft <= 1;
    }


    private string GetRoundWinner()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Instance.activeSelf)
                return "Player";
        }
        for (int i = 0; i < m_drivers.Count; i++)
        {
            if (m_drivers[i].gameObject.activeSelf)
                return "AI";
        }

        return "null";
    }


    private string GetGameWinner()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                return "Player";
        }
        for (int i = 0; i < m_drivers.Count; i++)
        {
            if (m_drivers[i].m_Wins == m_NumRoundsToWin)
                return "AI";
        }

        return "null";
    }


    private string EndMessage()
    {
        string message = "DRAW!";

        if (m_RoundWinner == "Player")
        {
            message = m_Tanks[0].m_ColoredPlayerText + " WINS THE ROUND!";
        }
        else if(m_RoundWinner == "AI")
        {
            message = m_drivers[0].m_ColoredPlayerText + " WINS THE ROUND!";
        }
            

        message += "\n\n\n\n";

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
        }
        for (int i = 0; i < m_drivers.Count; i++)
        {
            message += m_drivers[i].m_ColoredPlayerText + ": " + m_drivers[i].m_Wins + " WINS\n";
        }

        if (m_GameWinner == "Player")
        {
            message = m_Tanks[0].m_ColoredPlayerText + " WINS THE GAME!";
        }
        else if (m_GameWinner == "AI")
        {
            message = m_drivers[0].m_ColoredPlayerText + " WINS THE GAME!";
        }

        return message;
    }


    private void ResetAllTanks()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].Reset();
        }
    }

    private void RestoreInitial()
    {
        for (var i = 0; i < m_AITankCount; i++)
        {
            m_drivers[i].transform.position = m_initPosition[i].position;
            m_drivers[i].transform.rotation = m_initPosition[i].rotation;
            m_drivers[i].GetComponent<AITankManager>().Setup(i);
        }
    }


    private void EnableTankControl()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].EnableControl();
        }
    }


    private void DisableTankControl()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].DisableControl();
        }
    }
}