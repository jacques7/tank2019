﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGenerator : MonoBehaviour
{
    [SerializeField] private GameObject m_ItemBoxPrefab;
    [SerializeField] private LayerMask m_ObjectMask;

    private int m_ItemCount = 0;                    //今存在するアイテム数
    private float m_delta = 0.0f;
    private float m_span = 10.0f;                   //アイテム生成の時間

    private int m_NewItemPosX = 0;                  //新アイテム座標
    private int m_NewItemPosZ = 0;
    private bool m_LocationInvalid = false;         //新アイテム座標無効フラグ
    private float m_DetectRdius = 5.0f;             //新アイテムと他オブジェクトの間
    private int m_MaxItemCount = 3;                 //同時に存在するアイテム数

    // Update is called once per frame
    void Update()
    {
        this.m_delta += Time.deltaTime;
        if(this.m_delta > this.m_span)
        {
            this.m_delta = 0.0f;
            if(m_ItemCount < m_MaxItemCount)
            {
                m_ItemCount++;
                m_LocationInvalid = true;
                while (m_LocationInvalid)
                {
                    m_NewItemPosX = Random.Range(-40, 40);
                    m_NewItemPosZ = Random.Range(-40, 40);
                    Vector3 currentPosition = new Vector3(m_NewItemPosX, 1.0f, m_NewItemPosZ);
                    m_LocationInvalid = false;
                    //他のオブジェクトを探す
                    Collider[] colliders = Physics.OverlapSphere(currentPosition, m_DetectRdius, m_ObjectMask);
                    if(colliders.Length > 1)
                    {
                        //他のオブジェクトがあれば、また新座標を探す
                        m_LocationInvalid = true;
                    }
                }
                //座標が有効時、アイテムを生成
                GameObject itemBox = Instantiate(m_ItemBoxPrefab, new Vector3(m_NewItemPosX, 1, m_NewItemPosZ), Quaternion.identity) as GameObject;
            }
        }
    }

    public void ItemIsTaken()
    {
        //アイテムが拾われた
        this.m_ItemCount--;
    }
}
