﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArtificialTankDriver_by_QI
{
    public class TrainingUIController : MonoBehaviour
    {
        public Text label;
        public Image bar;
        public WorldController01 controller;
        //public WorldController02 controller;

        private void Update()
        {
            label.text = $"Epoch : {controller.Epoch}";
            bar.fillAmount = (float)controller.CurrentStepsInEpoch / controller.totalStepsPerEpoch;
        }
    }
}
