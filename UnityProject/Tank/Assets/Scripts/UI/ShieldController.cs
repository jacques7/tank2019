﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldController : MonoBehaviour
{
    private Quaternion m_RelativeRotation;

    public Slider m_Slider;

    // Start is called before the first frame update
    void Start()
    {
        m_RelativeRotation = m_Slider.transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        //　シールドの回転を禁じる
        transform.rotation = m_RelativeRotation;
    }
}
