﻿using System;
using UnityEngine;
using ALGORITHM.AI;

namespace ArtificialTankDriver_by_QI
{

    public class TankDriver : MonoBehaviour
    {

        public AITankManager target;
        public float viewRange = default;

        [HideInInspector] public int m_Wins;
        [HideInInspector] public string m_ColoredPlayerText;
        private Color m_AIColor;

        public GeneticOptimizeableNerualNetwork network;

        private void Awake()
        {
            target = GetComponent<AITankManager>();

            network = new GeneticOptimizeableNerualNetwork(7, 3);
            var actvationFunction = new TanhFunction();
            for (var i = 0; i < network.activateFunctions.Length; i++)
            {
                network.SetActivationFunctionForLayer(i, actvationFunction);
            }

            MeshRenderer[] renderers = gameObject.GetComponentsInChildren<MeshRenderer>();

            m_AIColor = renderers[0].material.color;
            m_ColoredPlayerText = "<color=#" + ColorUtility.ToHtmlStringRGB(m_AIColor) + ">COMPUTER " + "</color>";
        }

        public double CalculateFitness()
        {
            network.Fitness = target.m_Score;
            return network.Fitness;
        }

        //call per training update.
        public void DoSomethingUseful()
        {
            // calculate all input features

            var inputs = new double[7];
            var closestEnemy = target.ClosestEnemy(viewRange);
            var closestObstacle = target.ClosestObstacle(viewRange);
            //var closestBullet = target.ClosestBullet(viewRange);
            //var closestWall = target.ClosestWall(viewRange);

            //assuming that closest one is always the one it trying to attack.

            //distance between enemy.
            inputs[0] = closestEnemy != null ? Vector3.Distance(transform.position, closestEnemy.position) / viewRange : 1d;
            //cos to enemy.
            inputs[1] = closestEnemy != null ? Vector3.Dot(transform.right, (closestEnemy.position - transform.position).normalized) : 1d;
            //is weapon ready ?
            inputs[2] = target.m_WeaponReady ? 1d : 0d;
            // current speed.
            inputs[3] = target.m_Rigidbody.velocity.magnitude / target.m_MaxSpeed;
            // current torque.
            inputs[4] = target.m_Rigidbody.angularVelocity.magnitude / target.m_MaxTorque;
            //distance between obstacle
            inputs[5] = closestObstacle != null ? Vector3.Distance(transform.position, closestObstacle.position) / viewRange : 1d;
            //cos to obstacle
            inputs[6] = closestObstacle != null ? Vector3.Dot(transform.right, (closestObstacle.position - transform.position).normalized) : 1d;
            //distance between bullet.
            //inputs[7] = closestBullet != null ? Vector3.Distance(transform.position, closestBullet.position) / viewRange : 1d;
            //cos to bullet.
            //inputs[8] = closestBullet != null ? Vector3.Dot(transform.right, (closestBullet.position - transform.position).normalized) : 1d;
            //distance between wall
            //inputs[9] = closestWall != null ? Vector3.Distance(transform.position, closestWall.position) / viewRange : 1d;

            //feedforward
            var output = network.Compute(inputs);

            //drive
            target.SetMove((float)output[0]);
            target.SetRotate((float)output[1]);
            if (output[2] > 0) target.Shoot();
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, viewRange);
        }
    }

}

