﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ArtificialTankDriver_by_QI
{

    public class AITankManager : MonoBehaviour
    {
        public LayerMask m_EnemyMask;
        public LayerMask m_BulletMask;
        public LayerMask m_WallMask;
        public LayerMask m_ObstacleMask;
        public float m_MaxSpeed = default;
        public float m_MaxTorque = default;
        public float m_ShootCooldown = default;
        public float m_Score = default;

        public bool isAI = true;

        [HideInInspector] public GameObject m_Instance;

        private TankMovement m_Movement;
        private TankShooting m_Shooting;
        private TankHealth m_Health;
        private GameObject m_CanvasGameObject;

        public Rigidbody m_Rigidbody { get; private set; }
        private Collider m_Collider;

        public bool m_WeaponReady { get; private set; } = true;

        public int m_TankNum { get; private set; } = default;

        private float span = 1.0f;
        private float delta = 0;
        private int m_SamePositionCount = 0;
        private int m_SamePositionLimit = 300;
        private Transform m_OldTransform = default;

        public void Setup(int i)
        {
            m_WeaponReady = true;
            m_Score = 0;
            m_TankNum = i;

            m_Movement = gameObject.GetComponent<TankMovement>();
            m_Shooting = gameObject.GetComponent<TankShooting>();
            m_Health = gameObject.GetComponent<TankHealth>();
            m_CanvasGameObject = gameObject.GetComponentInChildren<Canvas>().gameObject;

            m_MaxSpeed = gameObject.GetComponent<TankMovement>().m_Speed;
            m_MaxTorque = gameObject.GetComponent<TankMovement>().m_TurnSpeed;

            m_Health.m_CurrentHealth = m_Health.m_StartingHealth;
            m_Health.SetHealthUI();

            gameObject.SetActive(true);
        }

        private void Start()
        {
            m_Rigidbody = gameObject.GetComponent<Rigidbody>();
            m_Collider = gameObject.GetComponent<Collider>();
            m_OldTransform = gameObject.transform;
            m_SamePositionCount = 0;
        }

        private void Update()
        {
            m_Score = m_Shooting.m_ScoreCallback;
            m_Movement.EngineAudio();

            if(m_OldTransform.position == gameObject.transform.position)
            {
                m_SamePositionCount++;
                if(m_SamePositionCount > m_SamePositionLimit)
                {
                    //m_Shooting.m_ScoreCallback--;
                }
                m_OldTransform = gameObject.transform;
            }
            else
            {
                m_SamePositionCount = 0;
            }
        }

        public void Score(float v)
        {
            m_Score += v;
        }

        public void DisableControl()
        {
            m_Movement.enabled = false;
            m_Shooting.enabled = false;

            m_CanvasGameObject.SetActive(false);
        }


        public void EnableControl()
        {
            m_Movement.enabled = true;
            m_Shooting.enabled = true;

            m_CanvasGameObject.SetActive(true);
        }

        public void SetMove(float dir)
        {
            m_Rigidbody.velocity = gameObject.transform.forward * m_MaxSpeed * dir;
        }

        public void SetRotate(float dir)
        {
            m_Rigidbody.angularVelocity = gameObject.transform.up * m_MaxTorque * dir;
        }

        public void Shoot()
        {
            if (!m_WeaponReady || !gameObject.activeSelf) return;
            m_WeaponReady = false;
            m_Shooting.AIFire();

            StartCoroutine(CooldownWeapon());
        }

        private IEnumerator CooldownWeapon()
        {
            yield return new WaitForSeconds(m_ShootCooldown);
            m_WeaponReady = true;
        }

        public Transform ClosestEnemy(float viewRange)
        {
            var cols = new List<Collider>(Physics.OverlapSphere(gameObject.transform.position, viewRange, m_EnemyMask));
            cols.Remove(m_Collider);
            var firstOrDefault = cols.OrderBy(x => Vector3.Distance(gameObject.transform.position, x.transform.position)).FirstOrDefault();
            return firstOrDefault != null ? firstOrDefault.transform : null;
        }

        public Transform ClosestBullet(float viewRange)
        {
            var cols = new List<Collider>(Physics.OverlapSphere(gameObject.transform.position, viewRange, m_BulletMask));
            //cols.Remove(m_Collider);
            var firstOrDefault = cols.OrderBy(x => Vector3.Distance(gameObject.transform.position, x.transform.position)).FirstOrDefault();
            return firstOrDefault != null ? firstOrDefault.transform : null;
        }
        //training 02//////////////////////////////////////////////////////////////////
        public Transform ClosestWall(float viewRange)
        {
            var cols = new List<Collider>(Physics.OverlapSphere(gameObject.transform.position, viewRange, m_BulletMask));
            //cols.Remove(m_Collider);
            var firstOrDefault = cols.OrderBy(x => Vector3.Distance(gameObject.transform.position, x.transform.position)).FirstOrDefault();
            return firstOrDefault != null ? firstOrDefault.transform : null;
        }

        public Transform ClosestObstacle(float viewRange)
        {
            var cols = new List<Collider>(Physics.OverlapSphere(gameObject.transform.position, viewRange, m_BulletMask));
            //cols.Remove(m_Collider);
            var firstOrDefault = cols.OrderBy(x => Vector3.Distance(gameObject.transform.position, x.transform.position)).FirstOrDefault();
            return firstOrDefault != null ? firstOrDefault.transform : null;
        }
        /// /////////////////////////////////////////////////////////////////////////
       

        public void Dead()
        {
            m_Score -= 5;
            m_Score = Mathf.Max(0, m_Score);
            gameObject.GetComponent<TankHealth>().OnDeath();
        }
    }

}