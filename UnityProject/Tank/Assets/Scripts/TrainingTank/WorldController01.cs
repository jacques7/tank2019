﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using ALGORITHM.AI;
using Random = UnityEngine.Random;

using System.IO;
using MiniJSON;
using System.Collections;

namespace ArtificialTankDriver_by_QI
{

    public class WorldController01 : MonoBehaviour
    {
        [SerializeField] private int m_GeneticNum = 0;
        public int stepsPerSecond = 1;
        public float physicStepLength = 0.02f;

        public int totalStepsPerEpoch = 1000;

        public int tankCount;
        public Transform generatePoint;
        public float generateRadius;
        public GameObject tankPrefab;
        
        public int CurrentStepsInEpoch { get; private set; }
        public int Epoch { get; private set; }

        private readonly List<Vector3> m_initPosition = new List<Vector3>();
        private readonly List<TankDriver> m_drivers = new List<TankDriver>();

        [HideInInspector] public GeneticOptimisation m_evolver;

        public void GenerateInitial()
        {
            m_evolver = new GeneticOptimisation(tankCount, 0.15, SelectionMethod.Natural);

            for (var i = 0; i < tankCount; i++)
            {
                var rp = Random.insideUnitSphere;
                rp.y = 0;
                rp = generatePoint.position + rp.normalized * Random.Range(1, generateRadius);
                m_initPosition.Add(rp);

                m_drivers.Add(Instantiate(tankPrefab, rp, Quaternion.identity).GetComponent<TankDriver>());
                m_drivers[i].GetComponent<AITankManager>().Setup(i);
                m_evolver.population.Add(m_drivers[i].network);
            }

            //m_evolver.RandomizePopulation();
            GetPopulation();
        }

        private void GetPopulation()
        {
            GetGeneticFromFile();
        }

        private void GetGeneticFromFile()
        {
            // ローカルストレージパスを取得
            // windows環境だと [C: \Users\(ユーザー名)\AppData\LocalLow\DefaultCompany\(プロジェクト名) が指定される。
            string storagePath = Application.persistentDataPath;

            // jsonファイル名を定義
            const string nameJson = "localsave_geneticData01c.txt";

            // アクセスパスを作成
            string accessPath = $"{storagePath}/{nameJson}";

            // パスが存在するか確認
            if (!File.Exists(accessPath))
            {
                //return $"Path:\n{accessPath}\nNotFound!";
            }

            // jsonファイルを読み込む
            string json = File.ReadAllText(accessPath, System.Text.Encoding.UTF8);

            // デシリアライズを行い、Textエリア表示用に整形する。
            Deserialize(json);
        }

        private void Deserialize(string sStrJson)
        {
            // JSONデータは最初は配列から始まるので、Deserialize（デコード）した直後にリストへキャスト      
            var parsedValue = (IList)Json.Deserialize(sStrJson);
            string json1 = Json.Serialize(parsedValue[m_GeneticNum]);
            json1 = "[" + json1 + "]";
            var parsedValue1 = (IList)Json.Deserialize(json1);

            for(var i = 0; i < tankCount; i++)
            {
                foreach (IDictionary resultOne in parsedValue1)
                {
                    var temp = m_evolver.population[i].OptimizeableValues;
                    for (var j = 0; j < resultOne.Count - 1; j++)
                    {
                        temp[j] = (double)resultOne[$"OptimizeableValues[{j}]"];
                    }
                    m_evolver.population[i].OptimizeableValues = temp;
                }
            }            
        }

        public void Evolve()
        {
            var fitnesses = new double[tankCount];
            for (var i = 0; i < m_drivers.Count; i++)
            {
                fitnesses[i] = m_drivers[i].CalculateFitness();
            }
            var max = new List<double>(fitnesses).OrderByDescending(x => x).FirstOrDefault();
            Debug.Log($"<color=#E91E63>Epoch {Epoch} finished with highest fitnesses {max}.</color>");

            m_evolver.Evolve(fitnesses);
            for (var i = 0; i < m_drivers.Count; i++)
            {
                m_drivers[i].network = m_evolver.population[i] as GeneticOptimizeableNerualNetwork;
            }

            Epoch++;
            RestoreInitial();
        }

        public void RestoreInitial()
        {
            CurrentStepsInEpoch = 0;
            for (var i = 0; i < tankCount; i++)
            {
                m_drivers[i].transform.position = m_initPosition[i];
                m_drivers[i].GetComponent<AITankManager>().Setup(i);
            }
        }

        private void Start()
        {
            GenerateInitial();
        }

        private void Update()
        {
            Physics.autoSimulation = false;
            if (CurrentStepsInEpoch > totalStepsPerEpoch) Evolve();
            for (var i = 0; i < stepsPerSecond; i++)
            {
                TrainingUpdate();
            }
        }

        public void TrainingUpdate()
        {
            CurrentStepsInEpoch++;
            Physics.Simulate(physicStepLength);
            foreach (var tankDriver in m_drivers)
            {
                tankDriver.DoSomethingUseful();
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(generatePoint.position, generateRadius);
        }
    }

}

