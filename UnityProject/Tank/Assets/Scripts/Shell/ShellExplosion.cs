﻿using System;
using UnityEngine;
using ArtificialTankDriver_by_QI;

public class ShellExplosion : MonoBehaviour
{
    public LayerMask m_TankMask;
    public ParticleSystem m_ExplosionParticles;       
    public AudioSource m_ExplosionAudio;              
    public float m_MaxDamage = 100f;                  
    public float m_ExplosionForce = 1000f;            
    public float m_MaxLifeTime = 2f;                  
    public float m_ExplosionRadius = 5f;

    private Action<float> m_ScoreCallback;
    private int shootBy = default;


    private void Start()
    {
        Destroy(gameObject, m_MaxLifeTime);
    }

    public void Setup(Action<float> ScoreCallback, int i)
    {
        m_ScoreCallback = ScoreCallback;
        shootBy = i;
    }


    private void OnTriggerEnter(Collider other)
    {
        // Find all the tanks in an area around the shell and damage them.
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

        for(int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

            if (!targetRigidbody)
                continue;

            targetRigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_ExplosionRadius);

            TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();

            if (!targetHealth)
                continue;

            float damage = CalculateDamage(targetRigidbody.position);

            ///訓練の時この下のコードは必要ない
            var isKilled = targetHealth.TakeDamage(damage);
            ////////////////////////////////////////////////////////////////////////////////////////
            ///訓練用
            //AITankManager aiTankManager = targetRigidbody.GetComponent<AITankManager>();
            //if (shootBy == aiTankManager.m_TankNum)
            //{
            //    //m_ScoreCallback(isKilled ? -8 : -3);
            //}
            //else
            //{
            //    var isKilled = targetHealth.TakeDamage(damage);
            //    m_ScoreCallback(isKilled ? 5 : 1);
            //}
            /////////////////////////////////////////////////////////////////////////////////////////
        }

        m_ExplosionParticles.transform.parent = null;
        m_ExplosionParticles.Play();
        m_ExplosionAudio.Play();

        Destroy(m_ExplosionParticles.gameObject, m_ExplosionParticles.main.duration);
        Destroy(gameObject);
    }


    private float CalculateDamage(Vector3 targetPosition)
    {
        // Calculate the amount of damage a target should take based on it's position.
        Vector3 explosionToTarget = targetPosition - transform.position;

        float explosionDistance = explosionToTarget.magnitude;

        float relativeDistance = (m_ExplosionRadius - explosionDistance) / m_ExplosionRadius;

        float damage = relativeDistance * m_MaxDamage;

        damage = Mathf.Max(0f, damage);
       
        return damage;
    }
}