﻿using UnityEngine;
using UnityEngine.UI;
using ArtificialTankDriver_by_QI;

public class TankHealth : MonoBehaviour
{
    public float m_StartingHealth = 100f;          
    public Slider m_Slider;                        
    public Image m_FillImage;                      
    public Color m_FullHealthColor = Color.green;  
    public Color m_ZeroHealthColor = Color.red;    
    public GameObject m_ExplosionPrefab;

    // シールドHP値
    [HideInInspector] public float m_ShieldHealth;
    
    
    private AudioSource m_ExplosionAudio;          
    private ParticleSystem m_ExplosionParticles;   
    public float m_CurrentHealth;  
    private bool m_Dead;


    private void Awake()
    {
        m_ExplosionParticles = Instantiate(m_ExplosionPrefab).GetComponent<ParticleSystem>();
        m_ExplosionAudio = m_ExplosionParticles.GetComponent<AudioSource>();

        m_ExplosionParticles.gameObject.SetActive(false);
    }


    private void OnEnable()
    {
        m_CurrentHealth = m_StartingHealth;
        m_Dead = false;

        SetHealthUI();
    }
    

    public bool TakeDamage(float amount)
    {
        // Adjust the tank's current health, update the UI based on the new health and check whether or not the tank is dead.

        // シールドがある時、シールドのHPを減らす
        if (gameObject.GetComponent<TankItemManager>().m_HasShield)
        {
            m_ShieldHealth -= amount;
            if(m_ShieldHealth <= 0)
            {
                //　シールドのHPがゼロになる時、シールドを破壊する。
                gameObject.GetComponent<TankItemManager>().m_HasShield = false;
            }
        }
        else
        {
            m_CurrentHealth -= amount;
        }

        SetHealthUI();

        if(m_CurrentHealth <= 0f && !m_Dead)
        {
            if (gameObject.GetComponent<AITankManager>() != null) {
                gameObject.GetComponent<AITankManager>().Dead();
            }
            else
            {
                OnDeath();
            }
            //OnDeath();
            return true;
        }
        else
        {
            return false;
        }
    }

    public void TakeHealing()
    {
        m_CurrentHealth = m_StartingHealth;

        SetHealthUI();
    }


    public void SetHealthUI()
    {
        // Adjust the value and colour of the slider.
        m_Slider.value = m_CurrentHealth;

        m_FillImage.color = Color.Lerp(m_ZeroHealthColor, m_FullHealthColor, m_CurrentHealth / m_StartingHealth);
    }


    public void OnDeath()
    {
        // Play the effects for the death of the tank and deactivate it.
        m_Dead = true;

        m_ExplosionParticles.transform.position = transform.position;
        m_ExplosionParticles.gameObject.SetActive(true);

        m_ExplosionParticles.Play();

        m_ExplosionAudio.Play();

        gameObject.SetActive(false);
    }
}