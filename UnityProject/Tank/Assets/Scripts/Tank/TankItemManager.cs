﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankItemManager : MonoBehaviour
{
    [SerializeField] private GameObject m_shield;
    [HideInInspector] public bool m_HasShield = false;

    //private MeshRenderer m_Mesh;
    private float m_Delta = 0.0f;
    private float m_Span = 10.0f;　//　シールド最大存在時間

    // Start is called before the first frame update
    void Start()
    {
        // タンクが生成された時、シールドを無効にする。
        m_shield.SetActive(false);
        m_HasShield = false;
    }

    // Update is called once per frame
    void Update()
    {
        //　シールドの処理
        this.HandleOfShield();
    }

    public void HandleItem(string itemType)
    {
        //　アイテムを拾った処理
        switch (itemType)
        {
            case "health":
                gameObject.GetComponent<TankHealth>().TakeHealing();
                break;
            case "shield":
                m_shield.SetActive(true);
                m_HasShield = true;
                gameObject.GetComponent<TankHealth>().m_ShieldHealth = gameObject.GetComponent<TankHealth>().m_StartingHealth;
                break;
            case "weapon":
                gameObject.GetComponent<TankShooting>().WeaponChange();
                break;
            default:
                break;
        }

        // アイテムが一つ減らすことを記録
        GameObject m_ItemGenerator = GameObject.Find("ItemGenerator");
        m_ItemGenerator.GetComponent<ItemGenerator>().ItemIsTaken();
    }

    private void HandleOfShield()
    {
        //　シールドの処理
        if (m_HasShield)
        {
            this.m_Delta += Time.deltaTime;
            if (this.m_Delta > this.m_Span)
            {
                this.m_Delta = 0.0f;
                m_shield.SetActive(false);
                m_HasShield = false;
            }
        }
        else
        {
            m_shield.SetActive(false);
        }
    }
}
