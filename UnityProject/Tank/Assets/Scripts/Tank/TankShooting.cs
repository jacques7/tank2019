﻿using UnityEngine;
using UnityEngine.UI;
using ArtificialTankDriver_by_QI;

public class TankShooting : MonoBehaviour
{
    public int m_PlayerNumber = 1;       
    public Rigidbody m_Shell;            
    public Transform m_FireTransform;
    public Transform m_LeftFireTransform;
    public Transform m_RightFireTransform;
    public Slider m_AimSlider;           
    public AudioSource m_ShootingAudio;  
    public AudioClip m_ChargingClip;     
    public AudioClip m_FireClip;         
    public float m_MinLaunchForce = 15f; 
    public float m_MaxLaunchForce = 30f; 
    public float m_MaxChargeTime = 0.75f;

    
    private string m_FireButton;         
    private float m_CurrentLaunchForce;  
    private float m_ChargeSpeed;         
    private bool m_Fired;
    private bool m_WeaponChanged;  //武器変換フラグ
    private float m_Delta = 0.0f;  //時間記録
    private float m_Span = 10.0f;  //武器変換時間

    public float m_ScoreCallback;

    public AITankManager target;

    private void OnEnable()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
    }


    private void Start()
    {
        m_FireButton = "Fire" + m_PlayerNumber;

        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;

        m_WeaponChanged = false;

        m_ScoreCallback = 0;

        target = GetComponent<AITankManager>();
    }
    

    private void Update()
    {
        if (gameObject.GetComponent<AITankManager>().isActiveAndEnabled)
        {
            return;
        }
        // Track the current state of the fire button and make decisions based on the current launch force.
        m_AimSlider.value = m_MinLaunchForce;

        if(m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
        {
            // at max charge, not yet fired
            m_CurrentLaunchForce = m_MaxLaunchForce;
            if(m_WeaponChanged)
            {
                FireThreeShell();
            }
            else
            {
                Fire();
            }
        }
        else if (Input.GetButtonDown(m_FireButton))
        {
            // have we pressed fire for the first time?
            m_Fired = false;
            m_CurrentLaunchForce = m_MinLaunchForce;

            m_ShootingAudio.clip = m_ChargingClip;
            m_ShootingAudio.Play();
        }
        else if (Input.GetButton(m_FireButton))
        {
            // holding the fire button, not yet fired
            m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;

            m_AimSlider.value = m_CurrentLaunchForce;
        }
        else if(Input.GetButtonUp(m_FireButton) && !m_Fired)
        {
            //we released the buttton, having not fired yet
            if (m_WeaponChanged)
            {
                FireThreeShell();
            }
            else
            {
                Fire();
            }
        }

        //　武器変換状態保持時間を測る
        if (m_WeaponChanged)
        {
            this.m_Delta += Time.deltaTime;
            if(this.m_Delta > this.m_Span)
            {
                this.m_Delta = 0.0f;
                m_WeaponChanged = false;
            }
        }
    }


    private void Fire()
    {
        // Instantiate and launch the shell.
        //m_Fired = true;    //このコードをコメントすると、連射機能が付き

        Rigidbody shellInstance = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;

        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();

        m_CurrentLaunchForce = m_MinLaunchForce;
    }

    private void FireThreeShell()
    {
        // 武器変換時のモード、三つの砲弾
        Rigidbody shellInstanceCenter = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
        Rigidbody shellInstanceLeft = Instantiate(m_Shell, m_LeftFireTransform.position, m_LeftFireTransform.rotation) as Rigidbody;
        Rigidbody shellInstanceRight = Instantiate(m_Shell, m_RightFireTransform.position, m_RightFireTransform.rotation) as Rigidbody;

        shellInstanceCenter.velocity = m_CurrentLaunchForce * m_FireTransform.forward;
        shellInstanceLeft.velocity = m_CurrentLaunchForce * m_LeftFireTransform.forward;
        shellInstanceRight.velocity = m_CurrentLaunchForce * m_RightFireTransform.forward;

        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();

        m_CurrentLaunchForce = m_MinLaunchForce;
    }

    public void WeaponChange()
    {
        // 武器アイテムボックスを拾った時、武器を変換する。
        this.m_WeaponChanged = true;
    }

    public void AIFire()
    {
        Rigidbody shellInstance = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        shellInstance.GetComponent<ShellExplosion>().Setup(ScoreCallback, target.m_TankNum);

        shellInstance.velocity = m_MinLaunchForce * m_FireTransform.forward;

        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();
    }

    public void ScoreCallback(float v)
    {
        m_ScoreCallback += v;
    }
}