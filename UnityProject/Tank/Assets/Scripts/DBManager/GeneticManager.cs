﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MiniJSON;
using System.IO;
using ALGORITHM.AI;

namespace ArtificialTankDriver_by_QI
{
    public class GeneticManager : MonoBehaviour
    {
        [SerializeField] private Text _displayField = default;
        [SerializeField] public WorldController01 controller;
        //[SerializeField] public WorldController02 controller;

        private GeneticOptimisation m_evolver;
        public void OnClickGetJsonFromFile()
        {
            m_evolver = GetJsonFromFile();
        }

        public void OnClickSetJsonFromFile()
        {
            _displayField.text = "Saving, please wait...";
            m_evolver = controller.m_evolver;
            //ローカルに保存する json データを作る
            var geneticData = new List<Hashtable>();
            for (var i = 0; i < m_evolver.populationCount; i++)
            {
                var tmp = new Hashtable()
                {
                    {$"Fitness[{i}]", m_evolver.population[i].Fitness},                    
                };
                for (var j = 0; j < m_evolver.population[i].OptimizeableValues.Count; j++)
                {
                    tmp.Add($"OptimizeableValues[{j}]", m_evolver.population[i].OptimizeableValues[j]);
                }
                geneticData.Add(tmp);
            }            

            string json = Json.Serialize(geneticData); //エンコードしてjsonデータにする。

            bool ret = SetJsonFromFile(json);

            if (ret)
            {
                _displayField.text = "FileSave OK!";
            }
            else
            {
                _displayField.text = "FileSave NG!";
            }
        }

        private GeneticOptimisation GetJsonFromFile()
        {
            m_evolver = controller.m_evolver;
            // ローカルストレージパスを取得
            // windows環境だと [C: \Users\(ユーザー名)\AppData\LocalLow\DefaultCompany\(プロジェクト名) が指定される。
            string storagePath = Application.persistentDataPath;

            // jsonファイル名を定義
            const string nameJson = "localsave_geneticData02.txt";

            // アクセスパスを作成
            string accessPath = $"{storagePath}/{nameJson}";

            // パスが存在するか確認
            if (!File.Exists(accessPath))
            {
                //return $"Path:\n{accessPath}\nNotFound!";
            }

            // jsonファイルを読み込む
            string json = File.ReadAllText(accessPath, System.Text.Encoding.UTF8);

            // デシリアライズを行い、Textエリア表示用に整形する。
            return CommonLib.Deserialize(json, m_evolver);
        }

        /// <summary>
        /// Sets the json from file.
        /// </summary>
        /// <returns><c>true</c>, if json from file was set, <c>false</c> otherwise.</returns>
        /// <param name="json">Json.</param>
        private bool SetJsonFromFile(string json)
        {
            // ローカルストレージパスを取得
            // windows環境だと [C: \Users\(ユーザー名)\AppData\LocalLow\DefaultCompany\(プロジェクト名) が指定される。
            string storagePath = Application.persistentDataPath;

            // jsonファイル名を定義
            string nameJson = "localsave_geneticData01.txt";

            // アクセスパスを作成
            string accessPath = $"{storagePath}/{nameJson}";

            // jsonファイルを書き込む
            File.WriteAllText(accessPath, json, System.Text.Encoding.UTF8);

            return true;
        }
    }

    /// <summary>
    /// Common lib.
    /// </summary>
    public class CommonLib
    {
        /// <summary>
        /// Deserialize the json sample.
        /// 渡されたJsonをデコードし、サンプル表示領域に表示しやすいフォーマット(string)に変更して返す。
        /// ※実際のゲームでは、リストデータ等で返す事が多い。
        /// </summary>
        /// <returns>The json sample.</returns>
        /// <param name="sStrJson">S string json.</param>
        public static GeneticOptimisation Deserialize(string sStrJson, GeneticOptimisation tmpGenetic)
        {
            GeneticOptimisation responseData = tmpGenetic;

            // JSONデータは最初は配列から始まるので、Deserialize（デコード）した直後にリストへキャスト      
            var parsedValue = (IList)Json.Deserialize(sStrJson);

            // リストの内容はオブジェクトなので、辞書型の変数に一つ一つ代入しながら、処理
            foreach (IDictionary resultOne in parsedValue)
            {
                var num = parsedValue.IndexOf(resultOne);
                responseData.population[num].Fitness = (double)resultOne[$"Fitness[{num}]"];
                for(var i=0; i<responseData.population[num].OptimizeableValues.Count; i++)
                {
                    responseData.population[num].OptimizeableValues[i] = (double)resultOne[$"OptimizeableValues[{i}]"];
                }
            }

            return responseData;
        }
    }

}
