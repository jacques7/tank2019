﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class DBManagerMain : MonoBehaviour
{
    [SerializeField] private Text _displayArea = default;
    [SerializeField] private Text _inputName = default;
    [SerializeField] private Text _record = default;

    private int m_PlayerScore = default;
    private int m_AIScore = default;

    private List<MemberData> _memberList;

    private void Start()
    {
        _record.text = "5:0";
        m_PlayerScore = GlobalController.Instance.m_PlayerScore;
        m_AIScore = GlobalController.Instance.m_AIScore;
        _record.text = $"{m_PlayerScore}:{m_AIScore}";
    }

    public void OnClickHome()
    {
        SceneManager.LoadScene("Start");
    }

    public void OnClickClear()
    {
        _displayArea.text = " ";
    }

    public void OnClickUpload()
    {
        SetJsonFromWww();
    }

    public void OnClickShow()
    {
        _displayArea.text = "Please wait. Downloading...";
        GetJsonFromWebRequest();
    }

    private void SetJsonFromWww()
    {
        string targetURL = "http://localhost/tankrecordapi/fightwithplayerdb/setMessage";

        string name = _inputName.text;
        string record = _record.text;

        if(name == "")
        {
            _displayArea.text = "名前を入力してください！";
            return;
        }
        else
        {
            _displayArea.text = "Please wait. Uploading...";
        }

        StartCoroutine(SetMessage(targetURL, name, record, CallBackApiSuccess, CallbackWebRequestFailed));
    }

    private void GetJsonFromWebRequest()
    {
        StartCoroutine(
            DownloadJson(
                CallbackWebRequestSuccess,
                CallbackWebRequestFailed
            )
        );
    }

    private void CallbackWebRequestSuccess(string reponse)
    {
        //Json の内容を MemberData型のリストとしてデコードする。
        _memberList = MemberDataModel.DeserializeFromJson(reponse);

        _displayArea.text = "Download success!";

        ShowMemberList();
    }

    private void CallBackApiSuccess()
    {
        _displayArea.text = "Success";
    }

    private void CallbackWebRequestFailed()
    {
        // jsonデータ取得に失敗した
        _displayArea.text = "WebRequest Failed";
    }

    private IEnumerator SetMessage(string url, string name, string record, Action cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        form.AddField("record", record);

        var webRequest = UnityWebRequest.Post(url, form);

        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if(webRequest.error != null)
        {
            Debug.LogError(webRequest.error);
            if(cbkFailed != null)
            {
                cbkFailed();
            }
        }
        else if(webRequest.isDone)
        {
            if(cbkSuccess != null)
            {
                cbkSuccess();
            }
        }
    }

    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        UnityWebRequest www = UnityWebRequest.Get("http://localhost/tankrecordapi/fightwithplayerdb/getMessages");
        yield return www.SendWebRequest();
        if(www.error != null)
        {
            Debug.LogError(www.error);
            if(null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if(www.isDone)
        {
            Debug.Log($"Success:{www.downloadHandler.text}");
            if(null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }

    private void ShowMemberList()
    {
        string sStrOutput = "";

        if(null == _memberList)
        {
            sStrOutput = "no list !";
        }
        else
        {
            foreach(MemberData memberOne in _memberList)
            {
                sStrOutput += $"name:{memberOne.Name} record:{memberOne.Record} Date:{memberOne.Date} \n";
            }
        }

        _displayArea.text = sStrOutput;
    }
}
