﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneticRecord
{
    [SerializeField] public string Genetics = @"[{""OptimizeableValues[21]"":-0.8143847686305572,
                                                 ""OptimizeableValues[19]"":-0.68653906029022271,
                                                 ""OptimizeableValues[9]"":-0.316301456799871,
                                                 ""OptimizeableValues[2]"":-0.42658934389547876,
                                                 ""OptimizeableValues[16]"":-0.25431391748334931,
                                                 ""OptimizeableValues[15]"":0.25663734053104981,
                                                 ""OptimizeableValues[14]"":0.83542881386141721,
                                                 ""OptimizeableValues[13]"":0.63649939961568436,
                                                 ""OptimizeableValues[12]"":-0.48756047128120461,
                                                 ""OptimizeableValues[11]"":0.91342689232594665,
                                                 ""OptimizeableValues[10]"":-0.47458459831522037,
                                                 ""OptimizeableValues[4]"":-0.93886701852961774,
                                                 ""OptimizeableValues[17]"":-1.1403846205865893,
                                                 ""OptimizeableValues[8]"":0.91731450889134525,
                                                 ""OptimizeableValues[5]"":-0.45898464483161672,
                                                 ""OptimizeableValues[7]"":0.51793652284794345,
                                                 ""OptimizeableValues[1]"":0.21607140275466774,
                                                 ""OptimizeableValues[22]"":0.86644681024665315,
                                                 ""OptimizeableValues[23]"":0.34508112414976644,
                                                 ""OptimizeableValues[3]"":0.86577484657325532,
                                                 ""Fitness[9]"":23,
                                                 ""OptimizeableValues[20]"":0.56420957323359766,
                                                 ""OptimizeableValues[6]"":1.067000737910625,
                                                 ""OptimizeableValues[0]"":0.89905113722153529,
                                                 ""OptimizeableValues[18]"":-0.15345437971570264}]";
}
