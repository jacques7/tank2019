﻿public class MemberData
{
    public string Name { get; set; }
    public string Record { get; set; }
    public string Date { get; set; }

    public MemberData()
    {
        Name = "";
        Record = "";
        Date = "";
    }
}